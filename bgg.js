const axios = require('axios');
const bgg = require('bgg')({
  timeout: 10000, // timeout of 10s (5s is the default)

  // see https://github.com/cujojs/rest/blob/master/docs/interceptors.md#module-rest/interceptor/retry
  retry: {
    initial: 100,
    multiplier: 2,
    max: 15e3
  }
});
const parseString = require('xml2js').parseString;

class BGG {
  /**
   * Get a single page worth of items from a given geeklist.
   *
   * Note: This is the old v1 API from BGG and it returns XML (yay.) and often often often
   * fails to return anything at all.
   *
   * @param {*} id
   * @param {*} page
   */
  getGeeklist(id, page) {
    return axios
      .get(`https://www.boardgamegeek.com/xmlapi/geeklist/${id}?page=${page}`)
      .then(
        response => {
          let xml = response.data;

          return new Promise((resolve, reject) => {
            // Convert the XML response to JSON that is actually useful to somebody.
            parseString(xml, (err, result) => {
              if (err) {
                reject(err);
              }

              resolve(result);
            });
          });
        },
        error => {
          throw error;
        }
      );
  }

  // User
  getCollection(username = 'JohnMunsch') {
    return bgg('collection', { username, own: 1 }).then(
      collection => {
        return collection;
      },
      err => {
        console.error(err);
      }
    );
  }

  getWishlist(username = 'JohnMunsch') {
    return bgg('collection', { username, wishlist: 1 }).then(
      collection => {
        return collection;
      },
      err => {
        console.error(err);
      }
    );
  }

  // Thing - Game/Expansion/Etc.
  getThing(id = 68448) {
    return bgg('thing', { id, stats: 1 }).then(
      thing => {
        return thing;
      },
      err => {
        console.error(err);
      }
    );
  }
}

module.exports = new BGG();
