const CronJob = require("cron").CronJob;
const faktory = require("faktory-worker");

async function updateBggUserJob(bggUserName) {
  const client = await faktory.connect();

  const jid = await client.push({
    jobtype: "update-bgg-user",
    args: [bggUserName],
  });

  await client.close();
}

async function updateGeeklistJob(geeklistId, page) {
  const client = await faktory.connect();

  const jid = await client.push({
    jobtype: "update-geeklist",
    args: [geeklistId, page],
  });

  await client.close();
}

// Update on a regular schedule, plus one extra that kicks off as soon as we start the
// sequence.
// new CronJob({
//   cronTime: '0 8 * * * *',
//   onTick: () => {
//     updateBggUserJob('JohnMunsch');
//   },
//   start: true,
//   runOnInit: true
// });

new CronJob({
  cronTime: "0 */15 * * * *",
  onTick: () => {
    // https://www.boardgamegeek.com/geeklist/277427/virtual-flea-market-dfw-2020
    updateGeeklistJob(277427, 1);
  },
  start: true,
  runOnInit: true,
});
