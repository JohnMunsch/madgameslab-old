const assert = require('assert');
const scraping = require('../models/scraping');

describe('Scraping', () => {
  it('should remove a single strikethrough within a description', () => {
    assert.equal(scraping.removeStrikethrough(`Black Industries Edition, 2007
      box is a little scratched and split at one corner
      4 of the miniatures have been painted
      otherwise, it is complete and in good condition
     
      [size=18][-]$30[/-] $20[/size]`),
      `Black Industries Edition, 2007
      box is a little scratched and split at one corner
      4 of the miniatures have been painted
      otherwise, it is complete and in good condition
     
      [size=18] $20[/size]`);
  });

  it('should remove multiple strikethroughs within a description', () => {
    assert.equal(scraping.removeStrikethrough(`English First Edition 2015
      Excellent Condition
      
      Also [ImageID=2771160][thing=187203][/thing]
      and [thing=214841][/thing]
      
      Excellent condition.
      
      It all fits in one box (tightly), but I still have the second box if you want it. (I'd rather not if I don't have to as I'd be bringing it from Canada.)
      
      [size=18][-]$70[/-] [-]$50[/-] $40[/size]
      
      [size=7]+altname+ Simurgh & expansions -altname-[/size]`),
      `English First Edition 2015
      Excellent Condition
      
      Also [ImageID=2771160][thing=187203][/thing]
      and [thing=214841][/thing]
      
      Excellent condition.
      
      It all fits in one box (tightly), but I still have the second box if you want it. (I'd rather not if I don't have to as I'd be bringing it from Canada.)
      
      [size=18]  $40[/size]
      
      [size=7]+altname+ Simurgh & expansions -altname-[/size]`);
  });

  it('should leave unchanged a description with no strikethroughs', () => {
    assert.equal(scraping.removeStrikethrough(`English Revised Edition 3rd printing 2014
      decks are still in shrink
      
      Includes expansion: [thing=133772][/thing]
      
      [size=18]$15[/size]
      
      [size=7]+altname+ Boss Monster & expansion -altname-[/size]`),
      `English Revised Edition 3rd printing 2014
      decks are still in shrink
      
      Includes expansion: [thing=133772][/thing]
      
      [size=18]$15[/size]
      
      [size=7]+altname+ Boss Monster & expansion -altname-[/size]`);
  });

  it('should pick up an altname if one is provided', () => {
    assert.equal(scraping.scrapeName(`English Revised Edition 3rd printing 2014\n
      decks are still in shrink\n
      \n
      Includes expansion: [thing=133772][/thing]\n
      \n
      [size=18]$15[/size]\n
      \n
      [size=7]+altname+ Boss Monster & expansion -altname-[/size]`, 'Boss Monster: The Dungeon Building Card Game'),
      'Boss Monster & expansion');
  });

  it('should have the default name if no altname is provided.', () => {
    assert.equal(scraping.scrapeName(`Black Industries Edition, 2007
      box is a little scratched and split at one corner
      4 of the miniatures have been painted
      otherwise, it is complete and in good condition
     
      [size=18][-]$30[/-] $20[/size]`, 'Talisman (Revised 4th Edition)'), 'Talisman (Revised 4th Edition)');
  });

  it('will still get the right name after removing strikethroughs', () => {
    let updatedDescription = scraping.removeStrikethrough(`English First Edition 2015\\r\\nExcellent Condition\\r\\n\\r\\nAlso [ImageID=2771160][thing=187203][/thing]\\r\\nand [thing=214841][/thing]\\r\\n\\r\\nExcellent condition.\\r\\n\\r\\nIt all fits in one box (tightly), but I still have the second box if you want it. (I'd rather not if I don't have to as I'd be bringing it from Canada.)\\r\\n\\r\\n[size=18][-]$70[/-] [-]$50[/-] $40[/size]\\r\\n\\r\\n[size=7]+altname+ Simurgh & expansions -altname-[/size]`);

    assert.equal(scraping.scrapeName(updatedDescription, 'Simurgh'), 'Simurgh & expansions');
  });

  it('will pick up the right price after removing strikethroughs', () => {
    let updatedDescription = scraping.removeStrikethrough(`English First Edition 2015
      Excellent Condition
      
      Also [ImageID=2771160][thing=187203][/thing]
      and [thing=214841][/thing]
      
      Excellent condition.
      
      It all fits in one box (tightly), but I still have the second box if you want it. (I'd rather not if I don't have to as I'd be bringing it from Canada.)
      
      [size=18][-]$70[/-] [-]$50[/-] $40[/size]
      
      [size=7]+altname+ Simurgh & expansions -altname-[/size]`);

    assert.equal(scraping.scrapePrice(updatedDescription), '40');
  });

  it('will pick up an updated price on a sold item', () => {
    let updatedDescription = scraping.removeStrikethrough(`[-]Condition: New In Shrink
      
      $10[/-]
      
      Sold to Lance Friday
      [user=lancefriday945][/user]
      for $5`);

      assert.equal(scraping.scrapePrice(updatedDescription), '5')
  });
});
