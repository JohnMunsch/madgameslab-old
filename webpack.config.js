// const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: './app/js/hammurabi-app.component.js',
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  devtool: 'source-map',
  plugins: [
    // Remove any existing bundle file we may have generated previously.
    //    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      filename: 'hammurabi.html',
      template: 'app/hammurabi.template.html',
      hash: true
    })
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'app')
  }
};
