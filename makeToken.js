const jwt = require('jsonwebtoken');

require('dotenv').config({ path: 'variables.env' });

var claims = {
  iss: process.env.AUTH0_DOMAIN,
  aud: process.env.AUTH0_AUDIENCE,
  bggUserName: process.argv[2]
};

let token = jwt.sign(claims, process.env.JWTSECRET);

console.log(token);
