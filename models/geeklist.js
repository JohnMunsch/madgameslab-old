const mongoose = require('mongoose');
const scraping = require('./scraping');

mongoose.Promise = global.Promise;

const itemSchema = new mongoose.Schema({
  index: { type: Number },
  name: String,
  nameWithoutDiacritics: String,
  price: Number,
  auction: Boolean,
  sold: Boolean,
  firm: Boolean,
  removeItem: Boolean,
  id: Number,
  objectid: Number,
  user: String,
  date: Number,
  imageId: Number,
  description: String,
  timestamp: Number
});

const geeklistSchema = new mongoose.Schema({
  id: { type: Number, index: true, unique: true },
  title: String,

  items: [itemSchema]
});

// Note: Don't forget that we cannot use arrow functions for the Mongoose
// callbacks because we need Mongoose to assign "this".
itemSchema.pre('save', function(next) {
  if (this.isModified('description')) {
    // Remove anything the user has struck through first.
    let updatedDescription = scraping.removeStrikethrough(this.description);

    // Then pull out various elements from the description.
    this.set({
      name: scraping.scrapeName(updatedDescription, this.name),
      price: scraping.scrapePrice(updatedDescription),
      auction: scraping.scrapeAuction(updatedDescription),
      sold: scraping.scrapeSold(updatedDescription),
      firm: scraping.scrapeFirm(updatedDescription),
      removeItem: scraping.scrapeRemove(updatedDescription)
    });
  }

  // Generate derivative data from that supplied to us.
  if (this.isModified('name')) {
    // This is helpful when searching for games like Orleans which have accents
    // or other diacritics. We can search against a version of the name where
    // those have been removed.
    this.set({
      nameWithoutDiacritics: scraping.removeDiacritics(this.name)
    });
  }

  next();
});

module.exports = mongoose.model('Geeklist', geeklistSchema);
