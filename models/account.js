const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Neither the collection nor the wishlist gets manipulated in any way, only replaced.
// Note: Can't call the collection "collection" or we run into a reserved word for Mongoose
// (or maybe MongoDB, I forget which).
const accountSchema = new mongoose.Schema({
  id: String,
  userCollection: mongoose.Schema.Types.Mixed,
  userWishlist: mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('Account', accountSchema);
