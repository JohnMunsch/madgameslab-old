const strikethroughRegex = /\[-]([\s\S]*?)\[\/-]/g;
const altnameRegex = /\+altname\+ (.+) -altname-/;
const priceRegex = /\$(\d+)/;
const auctionRegex = /(auction)/i;
const soldRegex = /\b(sold)/i;
const firmRegex = /(\bfirm\b)/i;
const removeRegex = /\+remove\+/i;

// This code is all about handling the data returned from BoardGameGeek
// geeklists. In particular, it handles lots of special text which users
// often embed within their geeklist item descriptions.
module.exports.removeDiacritics = name => {
  return name.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

module.exports.removeStrikethrough = description => {
  return description.replace(strikethroughRegex, '');
};

module.exports.scrapeName = (description, name) => {
  // Try to find an altname within the description. If there isn't one, just return the default name.
  let matches = description.match(altnameRegex);

  return matches ? matches[1] : name;
};

module.exports.scrapePrice = description => {
  let matches = description.match(priceRegex);

  return matches ? matches[1] : 0;
};

module.exports.scrapeAuction = description => {
  let matches = description.match(auctionRegex);

  return matches !== null;
};

module.exports.scrapeSold = description => {
  let matches = description.match(soldRegex);

  return matches !== null;
};

module.exports.scrapeFirm = description => {
  let matches = description.match(firmRegex);

  return matches !== null;
};

module.exports.scrapeRemove = description => {
  let matches = description.match(removeRegex);

  return matches !== null;
};
