# Introduction

https://polymer.github.io/lit-html/
https://github.com/Polymer/lit-element/tree/master
https://github.com/jbielick/faktory_worker_node
https://github.com/krasimir/navigo

## Development

Run the front-end build process.

```bash
npm install
npm run webpack
```

And the back-end server.

```bash
npm start
```

## Deployment

Switch to the devops directory (it's a separate project) and run:

```bash
npm run madgameslab
```

Note: Nothing is going to work unless you have a variables.env file in your devops directory with information for connecting to MongoDB (mlab.com hosted) and for decoding the JavaScript Web Tokens you will receive from users.

## Running

Running the Faktory server (Note: Currently only run locally):

production:

```bash
docker run --rm -it -v faktory-data:/var/lib/faktory -e "FAKTORY_PASSWORD=some_password" -p 127.0.0.1:7419:7419 -p 127.0.0.1:7420:7420 contribsys/faktory:latest /faktory -b :7419 -w :7420 -e production
```

dev:

```bash
faktory
```

You can connect to the web based UI for the server here: http://localhost:7420/

Run the scraper:

```bash
npm run workers
npm run scheduler
```

Run the app (Note: This happens automatically via the Ansible deployment):

```bash
forever start server.js
```

Commands to stop existing jobs (Note: the number on the second command is the job number):

```bash
forever list
forever stop 0
```

## Generate A Token

Note: This will leave the new token on the Mac clipboard ready to paste into an email.

```bash
node makeToken.js JohnMunsch | pbcopy
```

## Testing

```bash
npm test
```
