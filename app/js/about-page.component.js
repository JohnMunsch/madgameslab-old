import { LitElement, css, html } from 'lit-element';

class AboutPage extends LitElement {
  static get styles() {
    return css`
      /* @import url('https://fonts.googleapis.com/css?family=Open+Sans|Oswald'); */

      :host {
        display: block;
        max-width: 1200px;
        margin-left: auto;
        margin-right: auto;
        margin-bottom: 20px;

        --ivy-league: #607848;
        --mossociety: #789048;
        --agrio: #c0d860;
        --aglet: #f0f0d8;
        --purple: #604848;

        --header-background-color: var(--purple);
        --header-text-color: var(--agrio);
        --headline-text-color: var(--purple);

        font-family: 'Open Sans', sans-serif;
        font-size: smaller;
      }

      h1,
      h2,
      h3 {
        font-family: 'Oswald', sans-serif;
        color: var(--headline-text-color);
      }

      p,
      div {
        font-family: 'Open Sans', sans-serif;
      }
    `;
  }

  render() {
    return html`
      <h1>About</h1>

      <p>
        The VFM index exists for the most obvious of reasons, which is that I
        wanted it for myself. The earliest versions had no UI at all, they just
        monitored the VFM and emailed me selected items when they showed up on
        the list. But the reason the current version exists is that I am using
        it as a fundraising vehicle for my favorite charity: Extra Life!
      </p>

      <p>
        It's run by the Children's Miracle Network and it raises money for
        non-profit children's hospitals (<a
          href="https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=5756"
          >ratings on Charity Navigator</a
        >).
      </p>

      <h3>Acknowledgements</h3>
      <ul>
        <li>
          <a href="https://thenounproject.com/icon/516351/"
            >King icon courtesy of Nenad Radojcic @ The Noun Project</a
          >
        </li>
        <li>
          <a href="https://thenounproject.com/icon/1269/"
            >Meeple icon courtesy of Johan H. W. Basberg @ The Noun Project</a
          >
        </li>
      </ul>
    `;
  }
}

customElements.define('about-page', AboutPage);
