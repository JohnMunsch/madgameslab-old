import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

import Model from './model';

class AccountPage extends LitElement {
  static get properties() {
    return {
      model: Object
    };
  }

  firstUpdated(changedProperties) {
    this.model.store.subscribe(() => {
      this.requestUpdate();
    });
  }

  _loggedIn(state) {
    if (state.account.userCollection && state.account.userWishlist) {
      return html`
        <h2>
          Wishlist
          <span class="edit-link"
            >(<a
              href="https://boardgamegeek.com/collection/user/${state.account
                .id}?wishlist=1&ff=1"
              >Edit your wishlist on BoardGameGeek</a
            >)</span
          >
        </h2>
        <bgg-item-list
          .items="${state.account.userWishlist.items.item}"
        ></bgg-item-list>

        <h2>
          Collection
          <span class="edit-link"
            >(<a
              href="https://boardgamegeek.com/collection/user/${state.account
                .id}?own=1&ff=1"
              >Edit your collection on BoardGameGeek</a
            >)</span
          >
        </h2>
        <bgg-item-list
          .items="${state.account.userCollection.items.item}"
        ></bgg-item-list>
      `;
    } else {
      return html``;
    }
  }

  _loggedOut(state) {
    return html`
      <p><b>Step 1</b>: Give to the Extra Life charity. Please give at least $10 (and you know you could give even more than that).
      It's a charity for non-profit children's hospitals run by The Children's Miracle Network 
      (<a href="https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=5756">ratings on Charity Navigator</a>).</p>

      <div>
        <div class="extra-life-box">
          <iframe src="https://www.extra-life.org/index.cfm?fuseaction=widgets.300x250thermo&participantID=370215" width="302" height="252" frameborder="0" scrolling="no"><a href="https://www.extra-life.org/index.cfm?fuseaction=donorDrive.participant&participantID=370215">Make a Donation!</a></iframe>
        </div>
        <div>You MUST enter your email when giving and provide your BoardGameGeek.com user name in the area that says, "Write an encouraging message for John Munsch"!</div>
      </div>

      <p><b>Step 2</b>: I'll email you an ID token which you can paste into the box below. When you do that, I'll pull 
      down your collection and wishlist from BGG every couple of hours and use it to annotate the items in the virtual 
      flea market index!</p>

      <textarea id="idToken" @change="${e =>
        this._changeHandler(e)}"></textarea>
      <div class="note">Note: If you regularly use more than one browser, you'll need to paste the same token into each one.</div>

      <p><b>Step 3</b>: This is the best part! Items in the list which are already in your collection will look like this:

      <img src="/img/In Collection.png">

      <div><b>instead of like this:</b></div>

      <img src="/img/Not In Collection.png">

      <div><b>And items which are on your wishlist will appear like this to make them easy to pick out:</b></div>

      <img src="/img/Wishlist.png">
      </p>

      <p>I don't plan to stop there. I want to add new search options which allow filtering only by user name or game name 
      (rather than both simultaneously like it is today). Whatever else I add within the next year you'll get it. Also, my 
      index is now being used by various VFMs across the country and not just the BGG.con one. So if you attend more than
      one big board game convention a year, you get even more value out of it!</p>

      <p>Note: I'm well aware that the graphic design of the page is pretty awful. If you're someone with talent in that
      area, please feel free to do some new designs for them or ever create some new CSS for the appropriate classes
      (.in-collection, .wishlist-priority-1, .wishlist-priority-2, etc.).</p>
    `;
  }

  _changeHandler(e) {
    this.model.setIdToken(e.target.value.trim());
  }

  createRenderRoot() {
    return this;
  }

  render() {
    if (this.model) {
      let state = this.model.store.getState();

      return html`
        <h1>Account</h1>

        ${state.account.id ? this._loggedIn(state) : this._loggedOut(state)}
      `;
    } else {
      return html``;
    }
  }
}

class BGGItemListElement extends LitElement {
  static get properties() {
    return { items: Array };
  }

  _priorityDescription(priority) {
    switch (priority) {
      case 1:
        return 'Must have';
      case 2:
        return 'Love to have';
      case 3:
        return 'Like to have';
      case 4:
        return 'Thinking about it';
      case 5:
        return `Don't buy this`;
    }
  }

  _renderPriority(item) {
    if (item.status.wishlistpriority) {
      return html`
        -
        ${item.status.wishlistpriority}/${this._priorityDescription(
          item.status.wishlistpriority
        )}
      `;
    } else {
      return null;
    }
  }

  _renderItem(item) {
    return html`
      <li>
        <span class="name">${unsafeHTML(item.name.text)}</span>
        (${item.yearpublished})${this._renderPriority(item)}
      </li>
    `;
  }

  render() {
    if (this.items) {
      return html`
        <style>
          :host {
            display: block;
          }
          :host([hidden]) {
            display: none;
          }

          ul {
            list-style: none;
          }

          .name {
            font-weight: bold;
          }
        </style>

        <ul>
          ${this.items.map(item => this._renderItem(item))}
        </ul>
      `;
    } else {
      return html``;
    }
  }
}

customElements.define('account-page', AccountPage);
customElements.define('bgg-item-list', BGGItemListElement);
