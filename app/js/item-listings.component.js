import { LitElement, html } from 'lit-element';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

import format from 'date-fns/format';

class ItemListings extends LitElement {
  static get properties() {
    return { items: { type: Array }, geeklistid: { type: Number } };
  }

  _priceNote(item) {
    if (item.auction) {
      return html`
        [Auc.]
      `;
    } else if (item.firm) {
      return html`
        [Firm]
      `;
    }
  }

  _formatListingLink(geeklistId, index) {
    return `http://www.boardgamegeek.com/geeklist/${geeklistId}/item/${index}#item${index}`;
  }

  _formatItemLink(id) {
    return `https://www.boardgamegeek.com/boardgame/${id}`;
  }

  _formatDate(date) {
    // Sep 4, 1986 8:30 PM
    return format(date, 'MMM D YYYY H:mm A');
  }

  _lineStyling(item) {
    let classes = ['listing-row'];

    if (!item.show) {
      classes.push('hidden');
    } else {
      if (item.sold) {
        classes.push('item-sold');
      }

      if (item.inCollection) {
        classes.push('in-collection');
      }

      if (item.inWishlist) {
        classes.push('in-wishlist');
        classes.push(`wishlist-priority-${item.wishlistPriority}`);
      }
    }

    return classes.join(' ');
  }

  _renderItem(item, geeklistId) {
    return html`
      <div class="${this._lineStyling(item)}"">
        <div class="listing-column">
          <a href="${this._formatListingLink(geeklistId, item.index)}">
          ${item.name}</a>&nbsp;<a href="${this._formatItemLink(
      item.objectid
    )}"><img src="./img/meeple.svg"></a>
        </div>
        <div class="price-column">
          $${item.price}${this._priceNote(item)}
        </div>
        <div class="user-column">
          ${item.user}<br>
          <span class="date-listed">${this._formatDate(item.date)}</span>
        </div>
        <div class="description-column description">
          ${unsafeHTML(item.htmlDescription)}
        </div>
      </div>`;
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      ${this.items.map(item => this._renderItem(item, this.geeklistid))}
    `;
  }
}

customElements.define('item-listings', ItemListings);
