import jwtDecode from 'jwt-decode';
import produce from 'immer';
import elasticlunr from 'elasticlunr';
import * as Redux from 'redux';
import * as localforage from 'localforage';

const LOAD_ACCOUNT = 'Load Account';
const LOAD_GEEKLIST = 'Load Geeklist';
const LOAD_GEEKLISTS = 'Load Geeklists';
const SET_IDTOKEN = 'Set ID Token';
const SET_IDTOKEN_VALID = 'Set ID Token Valid';

export function _basicSort(field, a, b, op = value => value) {
  let aValue = op(a[field]);
  let bValue = op(b[field]);

  if (aValue < bValue) {
    return -1;
  }

  if (aValue > bValue) {
    return 1;
  }

  // a must be equal to b
  return 0;
}

export default class Model {
  constructor() {
    const initialState = {
      account: {
        bggUserName: '',
        userCollection: null,
        userWishlist: null
      },
      geeklists: {},
      idToken: null,
      idTokenValid: false
    };

    this.store = Redux.createStore(
      this.reducer.bind(this),
      initialState,
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    this.indexes = new Map();
  }

  reducer(state, action) {
    switch (action.type) {
      case LOAD_ACCOUNT:
        return produce(state, draft => {
          draft.account = action.data.account;

          // If we have geeklists, mark each one with the collection and wishlist info.
          Object.values(draft.geeklists).forEach(geeklist => {
            this._markCollectionAndWishlist(
              geeklist,
              draft.account.userCollection,
              draft.account.userWishlist
            );
          });
        });
      case LOAD_GEEKLIST:
        return produce(state, draft => {
          // Generate stats for the geeklist.
          let stats = this._generateStats(action.data.geeklist);
          action.data.geeklist.stats = stats;

          // Note: This actually modifies the incoming data by adding an additional htmlDescription to
          // each geeklist item.
          this._translateBGGAnnotations(action.data.geeklist.items);

          // If we already have account data, mark the geeklist with collection and wishlist info.
          if (draft.account.userCollection && draft.account.userWishlist) {
            this._markCollectionAndWishlist(
              action.data.geeklist,
              draft.account.userCollection,
              draft.account.userWishlist
            );
          }

          // Replace the one geeklist which has changed.
          draft.geeklists[action.data.id] = action.data.geeklist;
        });
      case LOAD_GEEKLISTS:
        return produce(state, draft => {
          let geeklists = {};
          action.data.forEach(geeklist => {
            geeklists[geeklist.id] = geeklist;
          });
          draft.geeklists = geeklists;
        });
      case SET_IDTOKEN:
        return produce(state, draft => {
          draft.idToken = action.data.idToken;
          draft.idTokenValid = false;
        });
      case SET_IDTOKEN_VALID:
        return produce(state, draft => {
          draft.idTokenValid = action.data.idTokenValid;
        });
      default:
        return state;
    }
  }

  _generateStats(geeklist) {
    // Filter out everything that is unsold. Then sort the sold list to group by user.
    let sold = geeklist.items
      .filter(item => item.sold)
      .sort((a, b) => {
        return _basicSort('user', a, b, value => value.toLowerCase());
      });

    let stats = {
      numItems: geeklist.items.length,
      numSold: sold.length,
      numUnsold: geeklist.items.length - sold.length
    };

    // Count up sold items by user.
    if (sold.length) {
      // TODO: This should be done with a Map rather than an object.
      let soldCounts = {};

      soldCounts = sold.reduce((counts, item) => {
        if (counts[item.user]) {
          counts[item.user] = ++counts[item.user];
        } else {
          counts[item.user] = 1;
        }

        return counts;
      }, soldCounts);

      // Generate a list (array) of all users who have sold items and a count for
      // each.
      stats.soldCounts = Object.keys(soldCounts).map(key => {
        return {
          user: key,
          sold: soldCounts[key]
        };
      });
    }

    return stats;
  }

  _markCollectionAndWishlist(geeklist, collection, wishlist) {
    if (geeklist.items) {
      geeklist.items = geeklist.items.map(item => {
        // Mark the items in the geeklist based upon which ones you already have in your
        // collection and those which are in your geeklist.
        if (collection) {
          if (
            collection.items.item.find(collectionItem => {
              return collectionItem.objectid == item.objectid;
            })
          ) {
            item.inCollection = true;
          }
        }

        if (wishlist) {
          let foundItem = wishlist.items.item.find(wishlistItem => {
            return wishlistItem.objectid == item.objectid;
          });

          if (foundItem) {
            item.inWishlist = true;
            item.wishlistPriority = foundItem.status.wishlistpriority;
          }
        }

        return item;
      });
    }
  }

  _translateBGGAnnotations(geeklist) {
    return geeklist.map((item, index) => {
      // The other part of annotation is a translation process for rendering to HTML. This could be
      // considered "not part of the model" because it is related to rendering of the data, but I choose
      // to do it here because it is very dependent upon the format of the data received from BGG (which
      // can and certainly has changed over time).
      //
      // This is necessary because BoardGameGeek doesn't use HTML tags nor
      // do they use Markdown in their entries. Instead it's a homegrown mess,
      // some of which is not provided with enough information to even completely
      // replace the markup with HTML (see the geeklist, thing, and user links, all of which
      // lack the useful bits like NAME(!!!!)).
      item.searchIndex = index;
      item.htmlDescription = item.description
        .replace(/\[-\]/gi, '<s>')
        .replace(/\[\/-\]/gi, '</s>')
        .replace(/\[b\]/gi, '<b>')
        .replace(/\[\/b\]/gi, '</b>')
        .replace(/\[i\]/gi, '<em>')
        .replace(/\[\/i\]/gi, '</em>')
        .replace(/\[u\]/gi, '<u>')
        .replace(/\[\/u\]/gi, '</u>')
        .replace(/\[size=(\w+)\]/gi, '<span style="font-size: $1px">')
        .replace(/\[\/size\]/gi, '</span>')
        .replace(/\[color=(#\w+)\]/gi, '<span style="color: $1">')
        .replace(/\[\/color\]/gi, '</span>')
        .replace(/\[url=([^\]]+)\]/gi, '<a href="$1">')
        .replace(/\[\/url\]/gi, '</a>')
        .replace(
          /\[geeklist=(\w+)\]/gi,
          '<a href="https://www.boardgamegeek.com/geeklist/$1">Other BGG Geeklist</a>'
        )
        .replace(/\[\/geeklist\]/gi, '')
        .replace(
          /\[thing=(\d+)\]/gi,
          '<a href="https://www.boardgamegeek.com/boardgameexpansion/$1">Expansion</a>'
        )
        .replace(/\[\/thing\]/gi, '')
        .replace(
          /\[user=(\w+)\]/gi,
          '<a href="https://www.boardgamegeek.com/user/$1">$1</a>'
        )
        .replace(/\[\/user\]/gi, '')
        .replace(
          /\[imageid=(\w+)\s*\]/gi,
          '<a href="https://www.boardgamegeek.com/image/$1">BGG Image</a>'
        );

      return item;
    });
  }

  _chronologicalSort(a, b) {
    return _basicSort('index', a, b);
  }

  _alphaItemSort(a, b) {
    return _basicSort('name', a, b);
  }

  _alphaUserSort(a, b) {
    return _basicSort('user', a, b);
  }

  _priceSort(a, b) {
    return _basicSort('price', a, b);
  }

  _loadAccount(idToken, bggUserName) {
    // Get the user's account data.
    let config = {
      headers: { Authorization: `Bearer ${idToken}` }
    };

    fetch(`/api/account/${bggUserName}`, config)
      .then(response => {
        return response.json();
      })
      .then(account => {
        this.store.dispatch({
          type: LOAD_ACCOUNT,
          data: {
            account
          }
        });
      });
  }

  // Helpers
  filterAndSortGeeklist(
    geeklist,
    searchText,
    soldStatus,
    sortOrder,
    collectionStatus
  ) {
    function removeDiacritics(builder) {
      // Define a pipeline function that removes diacritics (for example, Orléans becomes Orleans).
      let pipelineFunction = function(token) {
        if (token.metadata.fields.includes('name')) {
          return token.update(function() {
            return token
              .toString()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '');
          });
        } else {
          return token;
        }
      };

      // Register the pipeline function so the index can be serialised.
      lunr.Pipeline.registerFunction(pipelineFunction, 'removeDiacritics');

      // Add the pipeline function to both the indexing pipeline and the
      // searching pipeline
      builder.pipeline.before(lunr.stemmer, pipelineFunction);
      builder.searchPipeline.before(lunr.stemmer, pipelineFunction);
    }

    if (!geeklist || !geeklist.id) {
      return geeklist;
    } else {
      // Filter the list of items.
      return produce(geeklist, draft => {
        // Look to see if we've already built an index for this geeklist. If so, we'll use that.
        let index = this.indexes.get(geeklist.id);

        if (!index) {
          // Otherwise, we need to create one.
          index = elasticlunr(function() {
            // this.use(removeDiacritics);

            this.setRef('searchIndex');
            this.addField('name');
            this.addField('user');
            this.addField('description');

            draft.items.forEach(function(item) {
              this.addDoc(item);
            }, this);
          });

          this.indexes.set(geeklist.id, index);
        }

        if (searchText) {
          let lunrResults = index.search(searchText, {
            fields: {
              name: { boost: 2 },
              user: { boost: 1 },
              description: { boost: 1 }
            },
            bool: 'OR',
            expand: true
          });

          // Go through the list of search matches and mark each one as an entry to show.
          lunrResults.forEach(result => {
            draft.items[parseInt(result.ref, 10)].show = true;
          });
        } else {
          draft.items.forEach(item => (item.show = true));
        }

        draft.items = draft.items.map(item => {
          // This prevents us from continuing to check items if we've already filtered them out for another reason.
          if (item.show) {
            if (
              (soldStatus == 'sold' && !item.sold) ||
              (soldStatus == 'unsold' && item.sold)
            ) {
              item.show = false;
            }
          }

          // Same as above. Don't keep filtering if we've already filtered it out.
          if (item.show) {
            if (
              (collectionStatus == 'inCollection' && !item.inCollection) ||
              (collectionStatus == 'inWishlist' && !item.inWishlist)
            ) {
              item.show = false;
            }
          }

          return item;
        });

        switch (sortOrder) {
          case 'oldestFirst':
            draft.items = draft.items.sort(this._chronologicalSort);
            break;
          case 'newestFirst':
            draft.items = draft.items.sort(this._chronologicalSort).reverse();
            break;
          case 'alphaItem':
            draft.items = draft.items.sort(this._alphaItemSort);
            break;
          case 'alphaItemDesc':
            draft.items = draft.items.sort(this._alphaItemSort).reverse();
            break;
          case 'alphaUser':
            draft.items = draft.items.sort(this._alphaUserSort);
            break;
          case 'alphaUserDesc':
            draft.items = draft.items.sort(this._alphaUserSort).reverse();
            break;
          case 'price':
            draft.items = draft.items.sort(this._priceSort);
            break;
          case 'priceDesc':
            draft.items = draft.items.sort(this._priceSort).reverse();
            break;
        }
      });
    }
  }

  // Actions
  init() {
    // If we can find an ID token in the local storage, move it over to localforage and then nuke the contents of
    // local storage.
    let lsIdToken = localStorage.getItem('idToken');

    if (lsIdToken) {
      localforage.setItem('idToken', lsIdToken);
      localStorage.clear();
    }

    localforage.getItem('idToken').then(idToken => {
      if (idToken) {
        this.store.dispatch({
          type: SET_IDTOKEN,
          data: {
            idToken
          }
        });

        // Parse the token to pull out the BGG user name.
        const payload = jwtDecode(idToken);

        this._loadAccount(idToken, payload.bggUserName);
      }
    });
  }

  getGeeklists() {
    if (!this.fetching) {
      this.fetching = true;

      fetch('/api/geeklists')
        .then(response => response.json())
        .then(geeklists => {
          this.fetching = false;

          this.store.dispatch({
            type: LOAD_GEEKLISTS,
            data: geeklists
          });
        });
    }
  }

  getGeeklist(id) {
    if (!this.fetching) {
      this.fetching = true;

      // Check for a cached copy of the geeklist.
      localforage.getItem(`geeklist:${id}`).then(cachedGeeklist => {
        if (cachedGeeklist !== null) {
          // Load the cached copy up in the store.
          this.store.dispatch({
            type: LOAD_GEEKLIST,
            data: {
              id,
              geeklist: cachedGeeklist
            }
          });
        }
      });

      // Retrieve the latest version of the geeklist from the API.
      fetch(`/api/geeklist/${id}`)
        .then(response => {
          return response.json();
        })
        .then(geeklist => {
          // Remove all of the items marked for removal (w/ +remove+) before we pass them
          // along to the store or we save the list.
          geeklist.items = geeklist.items.filter((item, index) => {
            // We're also pulling anything which looks corrupted (no _id, no user, etc.).
            return item && item.user && item._id && !item.removeItem;
          });

          this.store.dispatch({
            type: LOAD_GEEKLIST,
            data: {
              id,
              geeklist
            }
          });

          // Then cache the new data via localforage.
          localforage.setItem(`geeklist:${id}`, geeklist);

          this.fetching = false;
        });
    }
  }

  setIdToken(idToken) {
    localforage.setItem('idToken', idToken);

    this.store.dispatch({
      type: SET_IDTOKEN,
      data: {
        idToken
      }
    });

    fetch(`/api/validateToken`, {
      headers: { Authorization: `Bearer ${idToken}` }
    })
      .then(response => {
        return response.json();
      })
      .then(
        result => {
          // Parse the token to pull out the BGG user name.
          const payload = jwtDecode(idToken);

          this._loadAccount(idToken, payload.bggUserName);
        },
        error => {
          this.store.dispatch({
            type: SET_IDTOKEN_VALID,
            data: {
              idTokenValid: false
            }
          });
        }
      );
  }
}
