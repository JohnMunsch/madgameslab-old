import { LitElement, css, html } from 'lit-element';

export class DefaultPage extends LitElement {
  get properties() {
    // All of the properties of this component and a type for each (used when converting
    // attributes to property values).
    return { model: { type: Object } };
  }

  static get styles() {
    return css`
      :host {
        display: block;
        min-height: calc(100vh - 40px - 24.5px);
      }
    `;
  }

  firstUpdated(changedProperties) {
    this.model.store.subscribe(() => {
      this.requestUpdate();
    });
  }

  render() {
    let state = this.model.store.getState();
    let geeklists = Object.values(state.geeklists);

    return html`
      <h1>Virtual Flea Markets on BoardGameGeek.com</h1>
      <ul>
        ${geeklists.map(
          geeklist =>
            html`
              <li><a href="#/listings/${geeklist.id}">${geeklist.title}</a></li>
            `
        )}
      </ul>

      <h2>What is a Virtual Flea Market (VFM)?</h2>

      <p>
        A Virtual Flea Market is where people know they will be getting together
        at some point in the near future (for example, at a gaming convention)
        and they wish to buy and sell some items in advance. In a traditional
        flea market, you show up with everything you might want to sell and
        negotiate a price on the spot. However, if you don't sell the item, you
        transported it to and from the venue for no gain. Similarly, if you're a
        buyer, you may show up only to find that nothing available appeals to
        you.
      </p>

      <p>
        The virtual version of that has people list all of their items onto a
        list at BoardGameGeek.com. Then, buyers can make offers and negotiate a
        price. When the event where everyone gets together actually happens, an
        exchange will occur where everbody trades their money for the already
        sold games at the prearranged prices. Only sold items are brought and no
        negotiation happens at the event. It typically is done in an hour or
        less. Here's a video of the exchange from BGG.con 2018:
      </p>

      <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/TaGLrQDxPU8"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>

      <h3>Things You Might Want to do on the VFM</h3>

      <p>
        Note: This list is not nearly finished, it needs to have screenshots
        inserted and some clarification done. But it's a start.
      </p>

      <h4>I want to list an item for sale</h4>
      <ol>
        <li>Go to the geeklist in question.</li>
        <li>Log in to BGG so it will let you add an item.</li>
        <li>
          Click the “Add Item” button you see just below the VFM header: (image
          here)
        </li>
        <li>You’ll see a page like this: (image here)</li>
        <li>Pick the “Board Game” link: (image here)</li>
        <li>
          Type in the name of the game you want to list. If it is something
          which is not a board game in BGG’s database (dice, a dice tower,
          carrying bag, anything else on Earth), type in “Outside” and you
          should see a game named “Outside the scope of BGG”. Pick that item.
        </li>
        <li>
          If you need a different picture (for example, you have a different
          version than what the default picture shows), you can find it in the
          gallery and put that picture ID here: (Image here)
        </li>
        <li>
          Put in your description (if it is an unusual edition of the item,
          provide that info and describe the item’s condition) and a price (with
          a $ in front of the price please, for the indexer). You can use the
          formatting tools they provide to add images and link to expansions
          you’re including.
        </li>
        <li>
          If you are listing an “Outside the scope of BGG” item, you can add a
          chunk of text like this, "+altname+ Whatever You'd Like To Call Your
          Item -altname-", and the index will list it by that name for
          searching.
        </li>
      </ol>

      <h4>I want to bid on an item for sale</h4>
      <ol>
        <li>Follow the first two steps above.</li>
        <li>
          Find the item in the geek list you wish to bid on and click the
          “Comment” button.
        </li>
        <li>
          Enter in any question(s) you have (for example about edition,
          condition, or included components) or alternatively, list a price
          you’re willing to bid for the item.
        </li>
        <li>
          If the listing user accepts your bid, they will normally strikethrough
          all of the items description and put “Sold” on the item and list your
          name as the winning bidder.
        </li>
      </ol>

      <h4>I want to mark my item as sold</h4>
      <ol>
        <li>
          If you sell an item, add “Sold” to its description (geeklist items may
          be changed as often as necessary). Most users also add the name of the
          seller, final price, and often use strike through on the original
          description though none of that is a requirement. Do not delete your
          sold item from the list!
        </li>
      </ol>
    `;
  }
}

// Note: Your element must have a hypen in the name (for example, "hello-world"). It's a requirement
// so that our components don't collide with future additions to HTML.
customElements.define('default-page', DefaultPage);
