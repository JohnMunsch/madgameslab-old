import { LitElement, html } from 'lit-element';
import Navigo from 'navigo';

import './about-page.component';
import './account-page.component';
import './default-page.component';
import './listings-page.component';

import Model from './model';

/**
 * The parent app object retrieves all of the data (that is, it makes the API calls) and also is
 * responsible for things like filtering, sorting, and other data operations like getting stats.
 *
 * All of the rendering and interaction is pushed down to the listings page, account page, etc.
 */
class HammurabiApp extends LitElement {
  constructor() {
    super();

    this.model = new Model();
    this.model.init();
  }

  static get properties() {
    return {
      model: Object
    };
  }

  firstUpdated(changedProperties) {
    let root = null;
    let useHash = true;
    this.router = new Navigo(root, useHash);

    const outlet = document.getElementById('outlet');

    this.router
      .on({
        '/about': () => {
          this.createPage('about-page', outlet);
        },
        '/account': () => {
          this.createPage('account-page', outlet);
        },
        '/listings/:geeklistId': (params, query) => {
          let geeklistId = parseInt(params.geeklistId, 10);
          this.model.getGeeklist(geeklistId);

          let pageElement = this.createPage('listings-page', outlet);
          pageElement.geeklistId = geeklistId;

          this.geeklistId = geeklistId;
        },
        '*': () => {
          // This is the default route if no other route matches.
          this.model.getGeeklists();

          this.createPage('default-page', outlet);
        }
      })
      .resolve();
  }

  createPage(elementName, outlet) {
    if (outlet) {
      // Create a component for the page and then pass our Redux model to the page component.
      let pageElement = document.createElement(elementName);
      pageElement.model = this.model;

      // Remove existing child node(s) inside the outlet.
      outlet.innerHTML = '';

      // Put our new element inside the outlet.
      outlet.appendChild(pageElement);

      return pageElement;
    }
  }

  // This turns off the Shadow DOM for this component.
  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <header>
        <div class="app-name">
          <a href="/hammurabi.html"
            ><img class="logo" src="../img/king.svg" />Hammurabi</a
          >
        </div>
        <div></div>
        <a href="#/account">Account</a>
        <a href="#/about">About</a>
      </header>

      <div id="outlet"></div>

      <footer>
        <div>
          Copyright &copy; 2018
          <a href="mailto:john.munsch@gmail.com">John Munsch</a>
        </div>
      </footer>
    `;
  }
}

customElements.define('hammurabi-app', HammurabiApp);
