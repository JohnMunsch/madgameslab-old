import { LitElement, html } from 'lit-element';
import { debounce } from 'lodash';

import './item-listings.component';
import './sold-listings.component';

class ListingsPage extends LitElement {
  static get properties() {
    return {
      model: Object,
      geeklist: Object,
      geeklistId: Number
    };
  }

  constructor() {
    super();

    this.searchText = '';
    this.soldStatus = 'unsold';
    this.sortOrder = 'oldestFirst';

    this._changeSearchTextDebounced = debounce(this._changeSearchText, 300, {
      trailing: true
    });
  }

  firstUpdated(changedProperties) {
    // Pull all of the search and sorting parameters and put them into variables for use
    // when filtering and sorting.
    let urlParams = new URLSearchParams(window.location.search);

    // Get values, if any, for the various filter and sort values.
    urlParams.forEach((value, key) => {
      this[key] = value;
    });

    this.model.store.subscribe(() => {
      this._filterChanged();
    });
  }

  _filterChanged() {
    this._updateFilterValues();

    let state = this.model.store.getState();

    // Go back to the original geeklist before filtering.
    if (this.geeklistId) {
      this.geeklist = state.geeklists[this.geeklistId];
    }

    // None of the search text, sold status, etc. are in the properties for this object because
    // we're triggering re-rendering manually. I'm still evaluating whether this is the best way or not.
    this.geeklist = this.model.filterAndSortGeeklist(
      this.geeklist,
      this.searchText,
      this.soldStatus,
      this.sortOrder,
      this.collectionStatus
    );

    this.requestUpdate();
  }

  // This echos all of the search parameters back into the URL so we can save off a search or go back
  // in history and end up where we last left off.
  _updateFilterValues() {
    // Pull all of the search and sorting parameters and put them into variables for use
    // when filtering and sorting.
    let urlParams = new URLSearchParams(window.location.search);

    if (this.searchText) {
      urlParams.set('searchText', this.searchText);
    } else {
      urlParams.delete('searchText');
    }

    // Don't add the default value to the query parameters, instead, just remove it from the params.
    if (this.soldStatus && this.soldStatus != 'unsold') {
      urlParams.set('soldStatus', this.soldStatus);
    } else {
      urlParams.delete('soldStatus');
    }

    // Don't add the default value to the query parameters, instead, just remove it from the params.
    if (this.sortOrder && this.sortOrder != 'oldestFirst') {
      urlParams.set('sortOrder', this.sortOrder);
    } else {
      urlParams.delete('sortOrder');
    }

    if (this.collectionStatus) {
      urlParams.set('collectionStatus', this.collectionStatus);
    } else {
      urlParams.delete('collectionStatus');
    }

    window.history.replaceState(
      null,
      '',
      `?${urlParams.toString()}${window.location.hash}`
    );
  }

  createRenderRoot() {
    return this;
  }

  _extraLifeOptions() {
    let state = this.model.store.getState();

    if (!state.account.id) {
      return html`
        <option>
          Filter to your collection and wishlist items if you're an Extra Life
          contributor! See the Account page.
        </option>
      `;
    } else {
      return html`
        <option
          value="inCollection"
          ?selected="${this.collectionStatus == 'inCollection'}"
          ?disabled="${!state.account.id}"
          >In My Collection</option
        >
        <option
          value="inWishlist"
          ?selected="${this.collectionStatus == 'inWishlist'}"
          ?disabled="${!state.account.id}"
          >In My Wishlist</option
        >
      `;
    }
  }

  render() {
    if (this.model && this.geeklist) {
      return html`
        <h1>${this.geeklist.title}</h1>

        <div>
          <form>
            <label>Sold Status: </label>
            <select @change="${this._changeSoldStatus}">
              <option
                value="soldAndUnsold"
                ?selected="${this.soldStatus == 'soldAndUnsold'}"
                >Sold and Unsold</option
              >
              <option value="sold" ?selected="${this.soldStatus == 'sold'}"
                >Sold</option
              >
              <option value="unsold" ?selected="${this.soldStatus == 'unsold'}"
                >Unsold</option
              >
            </select>

            <label>Collection Status: </label>
            <select @change="${this._changeCollectionStatus}">
              <option value="" ?selected="${this.collectionStatus == ''}"
                >All Items</option
              >
              ${this._extraLifeOptions()}
            </select>

            <input
              value="${this.searchText}"
              @input="${this._changeSearchTextDebounced}"
              placeholder="Filter by Listing or User"
            />
          </form>
        </div>

        <div class="listing-row">
          <div class="listing-column">
            <a @click="${this._toggleListingSort}">
              Listing
              <i class="fa ${this._listingSortStyling(this.sortOrder)}"></i>
            </a>
          </div>
          <div class="price-column">
            <a @click="${this._togglePriceSort}">
              Price
              <i class="fa ${this._priceSortStyling(this.sortOrder)}"></i>
            </a>
          </div>
          <div class="user-column">
            <a @click="${this._toggleUserSort}">
              User <i class="${this._userSortStyling(this.sortOrder)} fa"></i
            ></a>
            and
            <a @click="${this._toggleDateSort}"
              >Date <i class="${this._dateSortStyling(this.sortOrder)} fa"></i
            ></a>
          </div>
          <div class="description-column">Description</div>
        </div>
        <item-listings
          .items="${this.geeklist.items}"
          .geeklistid="${this.geeklistId}"
        ></item-listings>

        <h2>Stats</h2>
        <div>
          <span class="count">Listed</span>: ${this.geeklist.stats.numItems}
        </div>
        <div>
          <span class="count">Sold</span>: ${this.geeklist.stats.numSold}
        </div>
        <div>
          <span class="count">Unsold</span>: ${this.geeklist.stats.numUnsold}
        </div>

        <h3>Sold (by User)</h3>

        <sold-listings
          .items="${this.geeklist.stats.soldCounts}"
          .geeklistid="${this.geeklistId}"
        ></sold-listings>
      `;
    } else {
      return html``;
    }
  }

  _listingSortStyling(sortOrder) {
    if (sortOrder == 'alphaItem') {
      return 'fa-sort-up';
    } else if (sortOrder == 'alphaItemDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _priceSortStyling(sortOrder) {
    if (sortOrder == 'price') {
      return 'fa-sort-up';
    } else if (sortOrder == 'priceDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _userSortStyling(sortOrder) {
    if (sortOrder == 'alphaUser') {
      return 'fa-sort-up';
    } else if (sortOrder == 'alphaUserDesc') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _dateSortStyling(sortOrder) {
    if (sortOrder == 'oldestFirst') {
      return 'fa-sort-up';
    } else if (sortOrder == 'newestFirst') {
      return 'fa-sort-down';
    } else {
      return 'fa-sort';
    }
  }

  _toggleListingSort() {
    // Update the sort order based on the current one.
    if (this.sortOrder == 'alphaItem') {
      this.sortOrder = 'alphaItemDesc';
    } else if (this.sortOrder == 'alphaItemDesc') {
      this.sortOrder = 'oldestFirst';
    } else {
      this.sortOrder = 'alphaItem';
    }

    this._filterChanged();
  }

  _togglePriceSort() {
    // Update the sort order based on the current one.
    if (this.sortOrder == 'price') {
      this.sortOrder = 'priceDesc';
    } else if (this.sortOrder == 'priceDesc') {
      this.sortOrder = 'oldestFirst';
    } else {
      this.sortOrder = 'price';
    }

    this._filterChanged();
  }

  _toggleUserSort() {
    // Update the sort order based on the current one.
    if (this.sortOrder == 'alphaUser') {
      this.sortOrder = 'alphaUserDesc';
    } else if (this.sortOrder == 'alphaUserDesc') {
      this.sortOrder = 'oldestFirst';
    } else {
      this.sortOrder = 'alphaUser';
    }

    this._filterChanged();
  }

  _toggleDateSort() {
    // Update the sort order based on the current one.
    if (this.sortOrder == 'oldestFirst') {
      this.sortOrder = 'newestFirst';
    } else if (this.sortOrder == 'newestFirst') {
      this.sortOrder = 'oldestFirst';
    } else {
      this.sortOrder = 'oldestFirst';
    }

    this._filterChanged();
  }

  _changeSoldStatus(e) {
    this.soldStatus = e.target.value;
    this._filterChanged();
  }

  _changeCollectionStatus(e) {
    this.collectionStatus = e.target.value;
    this._filterChanged();
  }

  _changeSearchText(e) {
    this.searchText = e.target.value;
    this._filterChanged();
  }
}

customElements.define('listings-page', ListingsPage);
