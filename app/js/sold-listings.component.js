import { LitElement, html } from 'lit-element';

class SoldListings extends LitElement {
  static get properties() {
    return { items: { type: Array }, geeklistid: { type: Number } };
  }

  _renderItem(item, geeklistId) {
    return html`
      <li>
        ${item.user}:
        <a
          target="_blank"
          href="/hammurabi.html?soldStatus=sold&sortOrder=alphaUser&searchText=${item.user}#/listings/${geeklistId}"
        >
          ${item.sold}
        </a>
      </li>
    `;
  }

  createRenderRoot() {
    return this;
  }

  render() {
    return html`
      <ul>
        ${this.items.map(item => this._renderItem(item, this.geeklistid))}
      </ul>
    `;
  }
}

customElements.define('sold-listings', SoldListings);
