const parse = require('date-fns/parse');
const faktory = require('faktory-worker');
const mongoose = require('mongoose');

const bgg = require('./bgg');
const Account = require('./models/account');
const Geeklist = require('./models/geeklist');
const triggerIftttMakerWebhook = require('./models/makerWebhook');
const scraping = require('./models/scraping');

let geeklistMap = new Map();

async function updateGeeklistJob(geeklistId, page) {
  const client = await faktory.connect();

  const jid = await client.push({
    jobtype: 'update-geeklist',
    args: [geeklistId, page]
  });

  await client.close();
}

async function sendNotificationJob(item, user) {
  const client = await faktory.connect();

  const jid = await client.push({
    jobtype: 'send-notification',
    args: [item.name, item.index]
  });

  await client.close();
}

async function sendNotification(event, key, value1, value2, value3) {
  await triggerIftttMakerWebhook(event, key, value1, value2, value3);
}

async function updateBggUser(bggUserName) {
  // Get the BGG user's collection and wishlist.
  let [collection, wishlist] = await Promise.all([
    bgg.getCollection(bggUserName),
    bgg.getWishlist(bggUserName)
  ]);

  if (
    (collection.message &&
      collection.message.includes('Please try again later for access.')) ||
    (wishlist.message &&
      wishlist.message.includes('Please try again later for access.'))
  ) {
    throw new Error('Collection and/or wishlist unavailable.');
  }

  // $t gets used as a key name in both the collection and wishlist returned by BGG. Our storage
  // solution is not OK with that so we need to convert "$t" keys to "text" instead.
  collection.items.item = collection.items.item.map(item => {
    return Object.assign({}, item, {
      name: {
        sortindex: item.name.sortindex,
        text: item.name.$t
      }
    });
  });

  wishlist.items.item = wishlist.items.item.map(item => {
    return Object.assign({}, item, {
      name: {
        sortindex: item.name.sortindex,
        text: item.name.$t
      }
    });
  });

  let account = null;

  // Then persist both of them to our MongoDB.
  await Account.findOne({ id: bggUserName }, (err, foundAccount) => {
    if (err) {
      return console.error(err);
    }

    if (foundAccount) {
      account = foundAccount;
      account.userCollection = collection;
      account.userWishlist = wishlist;

      console.log(`Found account.`);
    } else {
      account = new Account({
        id: bggUserName,
        userCollection: collection,
        userWishlist: wishlist
      });
      console.log(`New account created.`);
    }

    account.save((err, account) => {
      if (err) return console.error(err);

      console.log('account saved.', account);
    });
  });
}

async function getGeeklist(geeklistId, refreshFromMongo) {
  let geeklist = geeklistMap.get(geeklistId);

  // If we've already got a cached geeklist which maps via Mongoose to our MongoDB then we'll
  // return that. Otherwise...
  if (!geeklist || refreshFromMongo) {
    // Retrieve the latest data from MongoDB.
    geeklist = await Geeklist.findOne({ id: geeklistId });

    // Do we already have a geeklist started for this particular ID?
    if (geeklist) {
      console.log(
        `Found geeklist. Retrieved ${geeklist.items.length} stored Items`
      );
    } else {
      geeklist = new Geeklist({ id: geeklistId, title: '', items: [] });
      await geeklist.save();
      console.log(`New geeklist.`);
    }

    // Store it for quick retrieval.
    geeklistMap.set(geeklistId, geeklist);
  }

  return geeklist;
}

// Try up to three times to get the page of the geeklist. It often seems like the first call fails and then
// another call immediately after will succeed. This tries to hide those retries, which are different from
// what Faktory facilitates.
async function getGeeklistWithRetries(geeklistId, page) {
  let bggGeeklist = await bgg.getGeeklist(geeklistId, page);

  if (
    bggGeeklist.message &&
    bggGeeklist.message.includes('Please try again later for access.')
  ) {
    bggGeeklist = await bgg.getGeeklist(geeklistId, page);
  } else {
    return bggGeeklist;
  }

  if (
    bggGeeklist.message &&
    bggGeeklist.message.includes('Please try again later for access.')
  ) {
    bggGeeklist = await bgg.getGeeklist(geeklistId, page);
  }

  return bggGeeklist;
}

async function updateGeeklist(geeklistId, page) {
  let numPages = 1;
  let geeklist = await getGeeklist(geeklistId, page === 1);

  // Remove corrupted items.
  geeklist.items = geeklist.items.filter((item, index) => {
    return item && item.user && item._id && item.index;
  });

  await getGeeklistWithRetries(geeklistId, page).then(bggGeeklist => {
    let updatedCount = 0;
    let unchangedCount = 0;
    let newCount = 0;

    let timestamp = Date.now();

    if (
      bggGeeklist.message &&
      bggGeeklist.message.includes('Please try again later for access.')
    ) {
      console.log('updateGeeklist:end', geeklistId, page);

      throw new Error('Geeklist unavailable.');
    }

    numPages = Math.ceil(bggGeeklist.geeklist.numitems / 100.0);

    if (!geeklist.title || geeklist.title !== bggGeeklist.geeklist.title[0]) {
      geeklist.set({ title: bggGeeklist.geeklist.title[0] });
    }

    // Iterate through all the items we got from BGG for this page and turn
    // each one into an Item we created using Mogoose Schema.
    bggGeeklist.geeklist.item.forEach(item => {
      // Format the data.
      let formattedItem = {
        index: item.$.id,
        name: scraping.scrapeName(item.body[0], item.$.objectname),
        id: parseInt(item.$.id, 10),
        objectid: parseInt(item.$.objectid, 10),
        user: item.$.username,
        date: parse(item.$.postdate).valueOf(),
        imageId: parseInt(item.$.imageid, 10),
        description: item.body[0],
        timestamp
      };

      // Look to see if we can find the item among our already stored items.
      let existingItemIndex = null;
      let existingListing = geeklist.items.find((storedItem, index) => {
        if (
          !storedItem ||
          !formattedItem ||
          !storedItem.index ||
          !formattedItem.index
        ) {
          console.log(
            'Item issues (stored, formatted):',
            storedItem,
            formattedItem
          );
        }

        existingItemIndex = index;
        return storedItem.index == formattedItem.index;
      });

      if (existingListing) {
        // If the item already exists, compare to see if it has changed in any way.
        if (
          existingListing.name !== formattedItem.name ||
          existingListing.id !== formattedItem.id ||
          existingListing.objectid !== formattedItem.objectid ||
          existingListing.user !== formattedItem.user ||
          existingListing.date !== formattedItem.date ||
          existingListing.imageId !== formattedItem.imageId ||
          existingListing.description !== formattedItem.description
        ) {
          // If so, update the store (and the timestamp as well).
          existingListing.name = formattedItem.name;
          existingListing.id = formattedItem.id;
          existingListing.objectid = formattedItem.objectid;
          existingListing.user = formattedItem.user;
          existingListing.date = formattedItem.date;
          existingListing.imageId = formattedItem.imageId;
          existingListing.description = formattedItem.description;
          existingListing.timestamp = formattedItem.timestamp;

          updatedCount++;

          geeklist.items.set(existingItemIndex, existingListing);
        } else {
          unchangedCount++;
        }
      } else {
        geeklist.items.addToSet(formattedItem);

        newCount++;
      }
    });

    console.log('item count: ', geeklist.items.length);
    console.log(
      `${geeklistId}:${page}: ${updatedCount}/${unchangedCount}/${newCount} (updated/unchanged/created)`
    );

    return geeklist.save().then(
      result => {
        console.log('updateGeeklist:end', geeklistId, page);

        return result;
      },
      reason => console.log(reason)
    );
  });

  // Page one is normally retrieved directly via a job injected from outside (cron or manual).
  // But all of the jobs for pages after page one are scheduled right here after we know how
  // many pages we need to retrieve.
  if (page == 1) {
    for (page = 2; page <= numPages; page++) {
      updateGeeklistJob(geeklistId, page);
    }
  }
}

function setupMongoose() {
  // Import environmental variables from our variables.env file
  require('dotenv').config({ path: 'variables.env' });

  // Connect to our Database and handle any bad connections
  var options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };
  mongoose.connect(process.env.LOADER, options);
  mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises
  mongoose.connection.on('error', err => {
    console.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
  });
}

async function registerJobHandlers() {
  faktory.register('send-notification', sendNotification);
  faktory.register('update-bgg-user', updateBggUser);
  faktory.register('update-geeklist', updateGeeklist);

  await faktory.work();
}

setupMongoose();
registerJobHandlers();

console.log('Waiting for work...');
