const compression = require("compression");
const express = require("express");
const expressJwt = require("express-jwt");
const mongoose = require("mongoose");

const Geeklist = require("./models/geeklist");
const Account = require("./models/account");

const app = express();

// This serves up all the static files. It's not used when we're
// hosted under Apache, but is when we're running locally.
app.use(express.static("app"));

(async () => {
  // Import environmental variables from our variables.env file.
  require("dotenv").config({ path: "variables.env" });

  app.use(compression());

  let jwtConfig = {
    secret: process.env.JWTSECRET,
    audience: process.env.AUTH0_AUDIENCE,
    issuer: process.env.AUTH0_DOMAIN,
    algorithms: ["HS256"]
  };

  var jwtCheck = expressJwt(jwtConfig);

  // Connect to our Database and handle any bad connections
  var options = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };
  mongoose.connect(process.env.READER, options);
  mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises
  mongoose.connection.on("error", err => {
    console.error(`🙅 🚫 🙅 🚫 🙅 🚫 🙅 🚫 → ${err.message}`);
  });

  app.get("/api/geeklist/:id", (req, res) => {
    // Retrieve the latest data from the store.
    let geeklistId = parseInt(req.params.id, 10);

    Geeklist.findOne({ id: geeklistId }).exec(function(err, geeklist) {
      if (err) return console.error(err);

      res.send(geeklist);
    });
  });

  app.get("/api/geeklists", (req, res) => {
    Geeklist.find(null, { _id: 1, id: 1, title: 1 })
      .exec()
      .then(
        results => {
          res.send(results);
        },
        reason => {
          res.status(500).send(reason);
        }
      );
  });

  // Note: The BGG related functions are restricted to an authenticated user.
  app.get("/api/account/:id", jwtCheck, (req, res) => {
    Account.findOne({ id: req.params.id }).exec(function(err, account) {
      if (err) return console.error(err);

      res.send(account);
    });
  });

  app.get("/api/validateToken", jwtCheck, (req, res) => {
    res.send({ valid: true });
  });

  app.listen(3000, () => {
    console.log("Hammurabi server: http://localhost:3000");
  });
})();
